#from cypress/browsers:latest
FROM node:10 AS ui-build
WORKDIR /app
#COPY ./cypress ./cypress
#COPY ./cypress.json ./cypress.json

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install app dependencies
COPY package.json ./
RUN npm install

# add app
COPY . ./
RUN npm run build

EXPOSE 3000
CMD ["npm", "start"]
# start app
#CMD sh -c 'npm start & npx cypress run --headless --browser chrome' 
